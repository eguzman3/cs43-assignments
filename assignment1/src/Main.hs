
module Main where

main :: IO ()
main = do
  putStrLn "hello world"

-- Pick three of the following.

-- Implement the map function using a fold.
map' :: (a -> b) -> [a] -> [b]  
map' f = foldr (\x acc -> f x : acc) []

-- Implement the filter function using a fold.
filter' :: (a -> Bool) -> [a] -> [a]
filter' f = foldr (\x acc -> if f x then x : acc else acc) []

-- Implement foldl using foldr.
myfoldl :: (a -> b -> a) -> a -> [b] -> a
myfoldl f acc xs = foldr (\acc2 g x -> g (f x acc2)) id xs acc

-- Write code to compute the smallest positive number that is evenly divisible by all the numbers from 1 to n. Provide an answer for n=20.

-- Write code to compute the nth prime number. Provide an answer for n=10001.
